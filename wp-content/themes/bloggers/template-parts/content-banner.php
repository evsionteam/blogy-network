<?php global $blogger_option; ?>
<div class="banner-slides">
	<div class="image-wrap" style="background-image:url('<?php echo $blogger_option['header_image']['url']; ?>')">
	</div>
	<div class="banner-content-wrap">
		<div class="banner-content">
			<h2>
				<div> 
					<?php echo $blogger_option['jumbo_btn_text']; ?>
				</div>
			</h2>
		</div>
	</div>
</div><!-- banner-slides -->

<div class="blg-welcome-info">
	<div class="row">
	<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
	<?php endif; ?>
	</div>
</div><!-- blg-welcome-info -->