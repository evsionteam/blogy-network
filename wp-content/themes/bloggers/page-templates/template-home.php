<?php
/* 
	Template Name: Homepage
*/

	get_header(); 
?>
<div id="page" class="site">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<header id="masthead" class="site-header header-main" role="banner">
				<div class="container">
					<div class="page-wrapper">
						<div class="row">
							<div class="col-sm-11">
								<h1>ALYCE MADDEN</h1>
								<ul class="tags">
									<li><a href="#">LIFE STYLE</a><span class="fa fa-circle"></span></li>
									<li><a href="#">MAKE-UP</a><span class="fa fa-circle"></span></li>
									<li><a href="#">FASHION</a></li>
								</ul>
							</div>
							<div class="col-sm-1">
								<div class="nav-menu"> 
									<button class="button-nav-toggle"><span class="icon">&#9776;</span></button>
								</div>
							</div>
						</div>
					</div>
				</div><!-- container -->	
			</header><!-- site-header -->

			<div class="menu">
			  <nav class="nav-main">
			    <div class="nav-container">
			      <ul>
			        <li><a href="">About Us</a></li>
			        <li><a href="">Services</a></li>
			        <li> <a href="#">Careers</a>
			          <ul>
			            <li><a href="#" class="back">Main Menu</a></li>
			            <li class="nav-label"><strong>JOBS IN AMERICA</strong></li>
			            <li><a href="">Accountant</a></li>
			            <li><a href="">Budget Analyst</a></li>
			            <li><a href="">Carpenter</a></li>
			            <li><a href="">Desktop Publisher</a></li>
			            <li><a href="">Electrical Engineer</a></li>
			            <li><a href="">Financial Analyst</a></li>
			            <li><a href="">Human Resources Specialists</a></li>
			            <li><a href="">Loan Officer</a></li>
			            <li><a href="">Reporter</a></li>
			            <li class="nav-label"><strong>JOBS IN GERMANY</strong></li>
			            <li><a href="">Budget Analyst</a></li>
			            <li><a href="">Carpenter</a></li>
			            <li><a href="">Electrical Engineer</a></li>
			            <li><a href="">Financial Analyst</a></li>
			            <li><a href="">Human Resources Specialists</a></li>
			            <li><a href="">Loan Officer</a></li>
			          </ul>
			        </li>
			        <li><a href="">Downloads</a></li>
			        <li><a href="">Contact</a></li>
			      </ul>
			    </div>
			  </nav>
			</div><!-- menu -->

			<div id="content" class="site-content">
				<div class="container">
					<div class="banner-slides">
						<div class="image-wrap" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/img/banner-img.jpg')">
						</div>
						<div class="banner-content-wrap">
							<div class="banner-content">
								<h2><a href="#"> A BLOG ABOUT MAKE UP AND FASHION</a></h2>
							</div>
						</div>
					</div><!-- banner-slides -->
			  

					<div class="blg-welcome-info">
						<div class="row">
							<div class="col-sm-4">
								<div class="category make-up">
									<a href="#">
										<figure>
											<img src="<?php echo get_template_directory_uri();?>/assets/img/make-up.jpg" alt="make-up">
											<figcaption class="image-caption">Make Up</figcaption>
										</figure>
									</a>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="category about-me">
									<a href="#">
										<figure>
											<img src="<?php echo get_template_directory_uri();?>/assets/img/abt-me.jpg" alt="abt-me">
											<figcaption class="image-caption">About Me</figcaption>
										</figure>
									</a>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="category life-style">
									<a href="#">
										<figure>
											<img src="<?php echo get_template_directory_uri();?>/assets/img/lifestyle.jpg" alt="lifestyle">
											<figcaption class="image-caption">LifeStyle</figcaption>
										</figure>
									</a>
								</div>
							</div>
						</div>
				    </div><!-- blg-welcome-info -->
		    
					<div class="blog-content-wrap">
					    <div class="row">
					    	<div class="col-sm-9">
					    		<div class="blog-post-list">
					    			<div class="blog-content-block">
					    				<article>
					    					<div class="post-title-wrap">
					    						<div class="post-category">
					    							<a href="#">MAKE UP</a>
					    						</div>
					    						<h2 class="post-title"><a href="#">STYLED BY ALYCE</a></h2>
					    						<div class="post-info"><span>July 22, 2016 17:28</span></div>
					    					</div>
					    					<div class="post-thumbnail"><a href="#">
					    						<img src="<?php echo get_template_directory_uri();?>/assets/img/post-image.jpg" alt="post-image"></a>
					    					</div>
					    					<div class="post-content-detail">
					    						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt.</p>
					    						<p>Ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
					    					</div>
					    				</article>
					    			</div>

					    			<div class="blog-detail-video">
						    			<div class="video-img">
						    				<a href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/avatar.jpg" alt="avatar"></a>
						    				<!-- <a href="#" class="wish-list"><span class="fa fa-heart-o">1</span></a> -->
						    			</div>
					    			</div>
					    		</div><!-- blog-post-list -->
					    	</div>

					    	<div class="col-sm-3">
					    		<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	    						<?php dynamic_sidebar( 'sidebar-1' ); ?>
								<?php endif; ?>
					    	</div>
					    </div>
				    </div><!-- blog-content-wrap -->
				</div>
			</div><!-- site-content -->
		</main><!-- #main -->
	</div><!-- #primary -->

	<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info">
				<a href="#">Annica Englund</a>
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
</div>  <!-- page -->
<?php get_footer(); ?>
