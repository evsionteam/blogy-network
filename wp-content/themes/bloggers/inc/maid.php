<?php
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action( 'wp_head', 'feed_links', 2 );
remove_action('wp_head', 'feed_links_extra', 3 );
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

add_filter( 'nav_menu_css_class', 'remove_unwanted_menu_classes' );
add_filter( 'nav_menu_item_id', 'remove_unwanted_menu_classes' );
add_filter( 'page_css_class', 'remove_unwanted_menu_classes' );
add_filter( 'body_class', 'remove_unwanted_body_class', 10, 2 );

function remove_unwanted_menu_classes( $classes ) {
    $allowed =  array (
        'current-menu-item',
        'current-menu-ancestor',
        'menu-item-has-children',
        'current-post-ancestor',
        'first',
        'last',
        'vertical',
        'horizontal'
    );

    if ( is_array( $classes ) && is_multi_author() ) {
        $classes[] = 'group-blog';
    }

    // Adds a class of hfeed to non-singular pages.
    if (is_array( $classes ) && ! is_singular() ) {
        $classes[] = 'hfeed';
       // array_push($classes, var);
    }
    
    if ( ! is_array( $classes ) )
        return $classes;

    foreach ( $classes as $key => $class ) {
        if ( in_array( $class, $allowed ) )
            continue;
        if ( 0 === strpos( $class, 'fa-' ) )
            continue;

        unset ( $classes[ $key ] );
    }

    return $classes;
}

function remove_unwanted_body_class( $wp_classes, $extra_classes ) {
    $whitelist = array( 'portfolio', 'home', 'error404' );
    $wp_classes = array_intersect( $wp_classes, $whitelist );
    return array_merge( $wp_classes, (array) $extra_classes );
}

