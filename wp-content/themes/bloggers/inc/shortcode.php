<?php

add_shortcode( 'post_archives', 'blogger_post_archive_shortcode' );

function blogger_post_archive_shortcode( $atts ){
	global $post, $withcomments;
	$withcomments = 1;
	
	$atts = shortcode_atts( array(
		'type' => ''
	), $atts, 'post_archives' );

	$args = array(
	    'category_name'    => $atts['type'],
	    'post_status'      => 'publish',  
	);
	$posts = get_posts( $args );

	$output = '';
	wp_reset_postdata();
		
	$js_data = array(
		'category_name' => $atts['type']
	);

	wp_localize_script( 'ajax-post-loader', 'BLOGGER_CATEGORY', $js_data );

	if(!$posts){
		ob_start();
			get_template_part( 'content', 'none' );
			$output .=  ob_get_contents();
		ob_end_clean();	
		return $output;
	}
	
	foreach( $posts as $post ){
		setup_postdata($post);
		ob_start();
			get_template_part( 'template-parts/content' );
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			$output .=  ob_get_contents();
		ob_end_clean();	
	}

	return $output;
}