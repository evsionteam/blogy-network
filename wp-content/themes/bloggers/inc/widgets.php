<?php

class My_Profile extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'my_profile_widget', // Base ID
			__( 'My Profile', 'blogger' ), // Name
			array( 'description' => __( 'Widget for you profile', 'blogger' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		if( ! empty( $instance['image_uri'] ) ){
			?>
			<div class="pro-img-wrap">
				<div class="pro-img" style="background-image: url( '<?php echo $instance['image_uri']; ?>' ); "></div>
			</div>
			<?php
			
		}
		if( ! empty( $instance['description'] ) ){
			echo "<div class='profile-desc'>";
			echo wpautop($instance['description']);
			echo "</div>";
		}

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'blogger' );
		$description = ! empty( $instance['description'] ) ? $instance['description'] : __( 'New description', 'blogger' );
		$image_uri = ! empty( $instance['image_uri'] ) ? $instance['image_uri'] : __( 'Image Url', 'blogger' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>">
				<?php _e( esc_attr( 'Description:' ) ); ?>
			</label> 
			<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>" 
			name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>" type="text" ><?php echo esc_attr( $description ); ?></textarea>
		</p>

		<p>
			<input type="text" class="widefat custom_media_url_<?php echo $this->get_field_id('image_uri'); ?>" name="<?php echo $this->get_field_name('image_uri'); ?>" 
				id="<?php echo $this->get_field_id('image_uri'); ?>" value="<?php echo $image_uri; ?>" style="margin-top:5px;">

			<input data-target=".custom_media_url_<?php echo $this->get_field_id('image_uri'); ?>" type="button" class="button button-primary custom_media_button" id="custom_media_button" 
			name="<?php echo $this->get_field_name('image_uri'); ?>" value="Upload Image" style="margin-top:5px;" />
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'], '<br><p><a><b><strong><i><em><u>' ) : '';
		$instance['image_uri'] = ( ! empty( $new_instance['image_uri'] ) ) ? strip_tags( $new_instance['image_uri'] ) : '';

		return $instance;
	}
}

class Follow_Me extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'follow_me_widget', // Base ID
			__( 'Follow Me', 'blogger' ), // Name
			array( 'description' => __( 'Follow me Widget', 'blogger' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		?>
		<div class="widget-social-icon">
			<ul class='blogger-social-link'>
				<?php if( !empty( $instance['fb'] ) ) : ?>
					<li>
						<a class="fb" href="<?php echo $instance['fb']; ?>">
							<span class="fa fa-facebook"></span>
						</a>
					</li>
				<?php endif; ?>	

				<?php if( !empty( $instance['twitter'] ) ) : ?>
					<li>
						<a class="twitter" href="<?php echo $instance['twitter']; ?>">
							<span class="fa fa-twitter"></span>
						</a>
					</li>
				<?php endif; ?>

				<?php if( !empty( $instance['google'] ) ) : ?>	
					<li>
						<a class="google" href="<?php echo $instance['google']; ?>">
							<span class="fa fa-google-plus"></span>
						</a>
					</li>
				<?php endif; ?>

				<?php if( !empty( $instance['instagram'] ) ) : ?>	
					<li>
						<a class="behance" href="<?php echo $instance['instagram']; ?>">
							<span class="fa fa-instagram"></span>
						</a>
					</li>
				<?php endif; ?>

				<?php if( !empty( $instance['behance'] ) ) : ?>
					<li>
						<a class="instagram" href="<?php echo $instance['behance']; ?>">
							<span class="fa fa-behance"></span>
						</a>
					</li>
				<?php endif; ?>
				
				<?php if( !empty( $instance['youtube'] ) ) : ?>
					<li>
						<a class="instagram" href="<?php echo $instance['youtube']; ?>">
							<span class="fa fa-youtube"></span>
						</a>
					</li>
				<?php endif; ?>
			</ul>
		</div>
		<?php
		if( ! empty( $instance['description'] ) ){
			echo "<div class='profile-desc'";
			echo $instance['description'];
			echo "</div>";
		}

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$fb = ! empty( $instance['fb'] ) ? $instance['fb'] : '';
		$twitter = ! empty( $instance['twitter'] ) ? $instance['twitter'] : '';
		$google = ! empty( $instance['google'] ) ? $instance['google'] : '';
		$instagram = ! empty( $instance['instagram'] ) ? $instance['instagram'] : '';
		$behance = ! empty( $instance['behance'] ) ? $instance['behance'] : '';
		$youtube = ! empty( $instance['youtube'] ) ? $instance['youtube'] : '';
	?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'fb' ) ); ?>"><?php _e( esc_attr( 'Facebook Link:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'fb' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'fb' ) ); ?>" type="text" value="<?php echo esc_attr( $fb ); ?>">
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>"><?php _e( esc_attr( 'Twitter Link:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ); ?>" type="text" value="<?php echo esc_attr( $twitter ); ?>">
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'google' ) ); ?>"><?php _e( esc_attr( 'Google Link:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'google' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'google' ) ); ?>" type="text" value="<?php echo esc_attr( $google ); ?>">
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>">
			<?php _e( esc_attr( 'Instagram Link:' ) ); ?>
		</label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'instagram' ) ); ?>" type="text" value="<?php echo esc_attr( $instagram ); ?>">
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'behance' ) ); ?>"><?php _e( esc_attr( 'Behance Link:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'behance' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'behance' ) ); ?>" type="text" value="<?php echo esc_attr( $behance ); ?>">
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>"><?php _e( esc_attr( 'Youtube Link:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'youtube' ) ); ?>" type="text" value="<?php echo esc_attr( $youtube ); ?>">
		</p>

		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['fb'] = ( ! empty( $new_instance['fb'] ) ) ? strip_tags( $new_instance['fb'] ) : '';
		$instance['twitter'] = ( ! empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : '';
		$instance['google'] = ( ! empty( $new_instance['google'] ) ) ? strip_tags( $new_instance['google'] ) : '';
		$instance['instagram'] = ( ! empty( $new_instance['instagram'] ) ) ? strip_tags( $new_instance['instagram'] ) : '';
		$instance['behance'] = ( ! empty( $new_instance['behance'] ) ) ? strip_tags( $new_instance['behance'] ) : '';
		$instance['youtube'] = ( ! empty( $new_instance['youtube'] ) ) ? strip_tags( $new_instance['youtube'] ) : '';

		return $instance;
	}
}

class Thumbnail_Category extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'thumbnail_category_widget', // Base ID
			__( 'Thumbnail Category', 'blogger' ), // Name
			array( 'description' => __( 'Category with Thumnail.', 'blogger' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];
		$term_id = ! empty( $instance['term_id'] ) ? $instance['term_id'] : 0;
		$image_uri = ! empty( $instance['image_uri'] ) ? $instance['image_uri'] : false;
		$term = get_term_by( 'id', $term_id , 'category' ); 
		?>
		<div class="category about-me">
			<?php if( $term && ! is_wp_error( $term) ): ?>
			<a href="<?php echo get_term_link($term); ?>">
				<?php if( $image_uri): ?>
					<figure style="background-image: url('<?php echo $image_uri ?>')">
						<figcaption class="image-caption"><?php echo $term->name; ?></figcaption>
					</figure>
				<?php endif; ?>	

			</a>
			<?php endif; ?>
		</div>
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$term_id = ! empty( $instance['term_id'] ) ? $instance['term_id'] : 0;
		$image_uri = ! empty( $instance['image_uri'] ) ? $instance['image_uri'] : '';
		$terms = get_terms( array( 'taxonomy' => 'category'));
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'term_id' ) ); ?>">
				<?php _e( 'Select Category', 'blogger' ); ?>
			</label>
			<?php if( isset( $terms ) ): ?>
				<select 
					id="<?php echo esc_attr( $this->get_field_id( 'term_id' ) ); ?>" 
					name="<?php echo esc_attr( $this->get_field_name( 'term_id' ) ); ?>">
					<?php foreach( $terms as $t ): ?>
						<option value="<?php echo $t->term_id; ?>" <?php selected( $t->term_id, $term_id ); ?>>
							<?php echo $t->name; ?>
						</option>
					<?php endforeach; ?>
				</select>
			<?php endif; ?>
		</p>

		
		<p>
			<input type="text" 
				id="cat_thumbnail_url" 
				class="widefat cat_thumbnail_url_<?php echo $this->get_field_id('image_uri'); ?>" 
				name="<?php echo $this->get_field_name('image_uri'); ?>" 
				id="<?php echo $this->get_field_id('image_uri'); ?>" 
				value="<?php echo $image_uri; ?>" 
				style="margin-top:5px;">

			<input type="button" 
				data-target=".cat_thumbnail_url_<?php echo $this->get_field_id('image_uri'); ?>" 
				class="button button-primary custom_media_button" 
				id="category_media_button" 
				value="Upload Image" 
				style="margin-top:5px;" />
		</p>
		
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['term_id'] = ( ! empty( $new_instance['term_id'] ) ) ? strip_tags( $new_instance['term_id'] ) : '';
		$instance['image_uri'] = ( ! empty( $new_instance['image_uri'] ) ) ? strip_tags( $new_instance['image_uri'] ) : '';
		return $instance;
	}
}

class Thumbnail_Page extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'thumbnail_page_widget', // Base ID
			__( 'Thumbnail Page', 'blogger' ), // Name
			array( 'description' => __( 'Page widget with Thumbnail.', 'blogger' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];
		$page_id = ! empty( $instance['page_id'] ) ? $instance['page_id'] : 0;
		$image_uri = ! empty( $instance['image_uri'] ) ? $instance['image_uri'] : false;
		$page = get_post( $page_id  ); 
		?>
		<div class="category about-me">
		<?php if( ! is_wp_error( $page) ): ?>
			<a href="<?php echo get_permalink( $page ); ?>">
				<?php if( $image_uri): ?>
					<figure style="background-image: url('<?php echo $image_uri ?>')">
						<figcaption class="image-caption"><?php echo $page->post_title; ?></figcaption>
					</figure>
				<?php endif; ?>	
			</a>
			<?php endif; ?>
		</div>
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$page_id = ! empty( $instance['page_id'] ) ? $instance['page_id'] : 0;
		$image_uri = ! empty( $instance['image_uri'] ) ? $instance['image_uri'] : '';
		$pages = get_pages(); 
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'page_id' ) ); ?>">
				<?php _e( 'Select Page', 'blogger' ); ?>
			</label>
			<?php if( isset( $pages ) ): ?>
				<select 
					id="<?php echo esc_attr( $this->get_field_id( 'page_id' ) ); ?>" 
					name="<?php echo esc_attr( $this->get_field_name( 'page_id' ) ); ?>">
					<?php foreach( $pages as $t ): ?>
						<option value="<?php echo $t->ID; ?>" <?php selected( $page_id, $t->ID ); ?>>
							<?php echo $t->post_title; ?>
						</option>
					<?php endforeach; ?>
				</select>
			<?php endif; ?>
		</p>

		
		<p>
			<input type="text" 
				id="page_thumbnail_url" 
				class="widefat page_thumbnail_url" 
				name="<?php echo $this->get_field_name('image_uri'); ?>" 
				id="<?php echo $this->get_field_id('image_uri'); ?>" 
				value="<?php echo $image_uri; ?>" 
				style="margin-top:5px;">

			<input type="button" 
				data-target=".page_thumbnail_url" 
				class="button button-primary custom_media_button" 
				id="category_media_button" 
				value="Upload Image" 
				style="margin-top:5px;" />
		</p>
		
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['page_id'] = ( ! empty( $new_instance['page_id'] ) ) ? strip_tags( $new_instance['page_id'] ) : '';
		$instance['image_uri'] = ( ! empty( $new_instance['image_uri'] ) ) ? strip_tags( $new_instance['image_uri'] ) : '';
		return $instance;
	}
}

class Blogger_Youtube_Channel extends WP_Widget {

	function __construct() { 
		parent::__construct(
			'blogger_youtube_channel', // Base ID
			__( 'Youtube Channel', 'blogger' ), // Name
			array( 'description' => __( 'Youtube Channel', 'blogger' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];
		$channel_id = ! empty( $instance['channel_id'] ) ? $instance['channel_id'] : false;
		$api_key = ! empty( $instance['api_key'] ) ? $instance['api_key'] : false;

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		echo do_shortcode('[Youtube_Channel_Gallery identify_by="channelid" user="'.$channel_id.'" key="'.$api_key.'" maxitems="9" thumb_columns_tablets="3" thumb_pagination="0" title="0" player="0" promotion="0" link_window="1"]');
		?>
		
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : false;
		$channel_id = ! empty( $instance['channel_id'] ) ? $instance['channel_id'] : false;
		$api_key = ! empty( $instance['api_key'] ) ? $instance['api_key'] : false;
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"> <?php _e( 'Title', 'blogger' ); ?></label>
			<input type="text" class="widefat"
				name="<?php echo $this->get_field_name( 'title'); ?>" 
				placeholder="<?php _e( 'Title', 'blogger' ); ?>"
				value="<?php echo $title; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('channel_id'); ?>"> <?php _e( 'Channel ID', 'blogger' ); ?></label>
			<input type="text" class="widefat"
				name="<?php echo $this->get_field_name( 'channel_id'); ?>" 
				placeholder="<?php _e( 'Youtube Channel ID', 'blogger' ); ?>"
				value="<?php echo $channel_id; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('api_key'); ?>"> <?php _e( 'API Key', 'blogger' ); ?></label>
			<input type="text" class="widefat"
				name="<?php echo $this->get_field_name( 'api_key'); ?>" 
				placeholder="<?php _e( 'API KEY', 'blogger' ); ?>"
				value="<?php echo $api_key; ?>" />
		</p>	
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['channel_id'] = ( ! empty( $new_instance['channel_id'] ) ) ? strip_tags( $new_instance['channel_id'] ) : '';
		$instance['api_key'] = ( ! empty( $new_instance['api_key'] ) ) ? strip_tags( $new_instance['api_key'] ) : '';
		return $instance;
	}
}
// register Foo_Widget widget
function register_my_profile() {
    register_widget( 'My_Profile' );
    register_widget( 'Follow_Me' );
    register_widget( 'Thumbnail_Category');
    register_widget( 'Thumbnail_Page');
    register_widget( 'blogger_instagram_widget' );
    register_widget( 'Blogger_Youtube_Channel' );

    unregister_widget( 'YoutubeChannelGallery_Widget' );
}
add_action( 'widgets_init', 'register_my_profile' );


function wpiw_init() {

	// define some constants
	// define( 'WP_INSTAGRAM_WIDGET_JS_URL', plugins_url( '/assets/js', __FILE__ ) );
	// define( 'WP_INSTAGRAM_WIDGET_CSS_URL', plugins_url( '/assets/css', __FILE__ ) );
	// define( 'WP_INSTAGRAM_WIDGET_IMAGES_URL', plugins_url( '/assets/images', __FILE__ ) );
	// define( 'WP_INSTAGRAM_WIDGET_PATH', dirname( __FILE__ ) );
	// define( 'WP_INSTAGRAM_WIDGET_BASE', plugin_basename( __FILE__ ) );
	// define( 'WP_INSTAGRAM_WIDGET_FILE', __FILE__ );

	// load language files
	load_plugin_textdomain( 'wp-instagram-widget', false, __FILE__ . '/languages/' );
}
add_action( 'init', 'wpiw_init' );

class blogger_instagram_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'blogger-instagram-feed',
			__( 'Instagram', 'wp-instagram-widget' ),
			array(
				'classname' => 'blogger-instagram-feed',
				'description' => esc_html__( 'Displays your latest Instagram photos', 'blogger' ),
				'customize_selective_refresh' => true
			)
		);
	}

	function widget( $args, $instance ) {

		$title = empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );
		$username = empty( $instance['username'] ) ? '' : $instance['username'];
		$limit = empty( $instance['number'] ) ? 9 : $instance['number'];
		$size = empty( $instance['size'] ) ? 'large' : $instance['size'];
		$target = empty( $instance['target'] ) ? '_self' : $instance['target'];
		$link = empty( $instance['link'] ) ? '' : $instance['link'];

		echo $args['before_widget'];

		if ( ! empty( $title ) ) { echo $args['before_title'] . wp_kses_post( $title ) . $args['after_title']; };

		do_action( 'wpiw_before_widget', $instance );

		if ( $username != '' ) {

			$media_array = $this->scrape_instagram( $username );

			if ( is_wp_error( $media_array ) ) {

				echo wp_kses_post( $media_array->get_error_message() );

			} else {

				// filter for images only?
				if ( $images_only = apply_filters( 'wpiw_images_only', FALSE ) ) {
					$media_array = array_filter( $media_array, array( $this, 'images_only' ) );
				}

				// slice list down to required limit
				$media_array = array_slice( $media_array, 0, $limit );

				// filters for custom classes
				$ulclass = apply_filters( 'wpiw_list_class', 'instagram-pics instagram-size-' . $size );
				$liclass = apply_filters( 'wpiw_item_class', '' );
				$aclass = apply_filters( 'wpiw_a_class', '' );
				$imgclass = apply_filters( 'wpiw_img_class', '' );
				$template_part = apply_filters( 'wpiw_template_part', 'parts/wp-instagram-widget.php' );

				?><ul class="clearfix <?php echo esc_attr( $ulclass ); ?>"><?php
				foreach ( $media_array as $item ) {
					// copy the else line into a new file (parts/wp-instagram-widget.php) within your theme and customise accordingly
					if ( locate_template( $template_part ) != '' ) {
						include locate_template( $template_part );
					} else {

						?>
						<li class="<?php echo esc_attr( $liclass ); ?>">
							<a href="<?php echo esc_url( $item['large'] );  ?>" 
								rel="prettyPhoto[pp_gal_blog_network]" 
								class="insta_blognetwork <?php echo esc_attr( $aclass ); ?>">
								<img 
									src="<?php echo esc_url( $item[$size] );  ?>"
									class="<?php echo esc_attr( $imgclass ); ?>"
									alt="<?php echo esc_attr( $item['description'] ); ?>" 
								/>
							</a>
						</li>
						<?php
						// echo '<li class="'. esc_attr( $liclass ) .'"><a href="'. esc_url( $item['link'] ) .'" target="'. esc_attr( $target ) .'"  class="'. esc_attr( $aclass ) .'"><img src="'. esc_url( $item[$size] ) .'"  alt="'. esc_attr( $item['description'] ) .'" title="'. esc_attr( $item['description'] ).'"  class="'. esc_attr( $imgclass ) .'"/></a></li>';
					}
				}
				?></ul><?php
			}
		}

		do_action( 'wpiw_after_widget', $instance );

		echo $args['after_widget'];
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => __( 'Instagram', 'wp-instagram-widget' ), 'username' => '', 'size' => 'large', 'link' => __( 'Follow Me!', 'wp-instagram-widget' ), 'number' => 9, 'target' => '_self' ) );
		$title = $instance['title'];
		$username = $instance['username'];
		$number = absint( $instance['number'] );
		$size = $instance['size'];
		$target = $instance['target'];
		$link = $instance['link'];
		?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'wp-instagram-widget' ); ?>: <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>"><?php esc_html_e( 'Username', 'wp-instagram-widget' ); ?>: <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>" type="text" value="<?php echo esc_attr( $username ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of photos', 'wp-instagram-widget' ); ?>: <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" /></label></p>
		<input type="hidden" name="<?php echo esc_attr( $this->get_field_name( 'size' ) ); ?>" value="thumbnail" />
		
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>"><?php esc_html_e( 'Open links in', 'wp-instagram-widget' ); ?>:</label>
			<select id="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'target' ) ); ?>" class="widefat">
				<option value="_self" <?php selected( '_self', $target ) ?>><?php esc_html_e( 'Current window (_self)', 'wp-instagram-widget' ); ?></option>
				<option value="_blank" <?php selected( '_blank', $target ) ?>><?php esc_html_e( 'New window (_blank)', 'wp-instagram-widget' ); ?></option>
			</select>
		</p>
		
		<?php

	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['username'] = trim( strip_tags( $new_instance['username'] ) );
		$instance['number'] = ! absint( $new_instance['number'] ) ? 9 : $new_instance['number'];
		$instance['size'] = ( ( $new_instance['size'] == 'thumbnail' || $new_instance['size'] == 'large' || $new_instance['size'] == 'small' || $new_instance['size'] == 'original' ) ? $new_instance['size'] : 'large' );
		$instance['target'] = ( ( $new_instance['target'] == '_self' || $new_instance['target'] == '_blank' ) ? $new_instance['target'] : '_self' );
		$instance['link'] = strip_tags( $new_instance['link'] );
		return $instance;
	}

	// based on https://gist.github.com/cosmocatalano/4544576
	function scrape_instagram( $username ) {

		$username = strtolower( $username );
		$username = str_replace( '@', '', $username );

		if ( false === ( $instagram = get_transient( 'instagram-a5-'.sanitize_title_with_dashes( $username ) ) ) ) {

			$remote = wp_remote_get( 'http://instagram.com/'.trim( $username ) );

			if ( is_wp_error( $remote ) )
				return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'wp-instagram-widget' ) );

			if ( 200 != wp_remote_retrieve_response_code( $remote ) )
				return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'wp-instagram-widget' ) );

			$shards = explode( 'window._sharedData = ', $remote['body'] );
			$insta_json = explode( ';</script>', $shards[1] );
			$insta_array = json_decode( $insta_json[0], TRUE );

			if ( ! $insta_array )
				return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );

			if ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
				$images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
			} else {
				return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );
			}

			if ( ! is_array( $images ) )
				return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'wp-instagram-widget' ) );

			$instagram = array();

			foreach ( $images as $image ) {

				$image['thumbnail_src'] = preg_replace( '/^https?\:/i', '', $image['thumbnail_src'] );
				$image['display_src'] = preg_replace( '/^https?\:/i', '', $image['display_src'] );

				// handle both types of CDN url
				if ( ( strpos( $image['thumbnail_src'], 's640x640' ) !== false ) ) {
					$image['thumbnail'] = str_replace( 's640x640', 's160x160', $image['thumbnail_src'] );
					$image['small'] = str_replace( 's640x640', 's320x320', $image['thumbnail_src'] );
				} else {
					$urlparts = wp_parse_url( $image['thumbnail_src'] );
					$pathparts = explode( '/', $urlparts['path'] );
					array_splice( $pathparts, 3, 0, array( 's160x160' ) );
					$image['thumbnail'] = '//' . $urlparts['host'] . implode( '/', $pathparts );
					$pathparts[3] = 's320x320';
					$image['small'] = '//' . $urlparts['host'] . implode( '/', $pathparts );
				}

				$image['large'] = $image['thumbnail_src'];

				if ( $image['is_video'] == true ) {
					$type = 'video';
				} else {
					$type = 'image';
				}

				$caption = __( 'Instagram Image', 'wp-instagram-widget' );
				if ( ! empty( $image['caption'] ) ) {
					$caption = $image['caption'];
				}

				$instagram[] = array(
					'description'   => $caption,
					'link'		  	=> trailingslashit( '//instagram.com/p/' . $image['code'] ),
					'time'		  	=> $image['date'],
					'comments'	  	=> $image['comments']['count'],
					'likes'		 	=> $image['likes']['count'],
					'thumbnail'	 	=> $image['thumbnail'],
					'small'			=> $image['small'],
					'large'			=> $image['large'],
					'original'		=> $image['display_src'],
					'type'		  	=> $type
				);
			}

			// do not set an empty transient - should help catch private or empty accounts
			if ( ! empty( $instagram ) ) {
				$instagram = base64_encode( serialize( $instagram ) );
				set_transient( 'instagram-a5-'.sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'blogger_instagram_cache_time', HOUR_IN_SECONDS*2 ) );
			}
		}

		if ( ! empty( $instagram ) ) {

			return unserialize( base64_decode( $instagram ) );

		} else {

			return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'wp-instagram-widget' ) );

		}
	}

	function images_only( $media_item ) {

		if ( $media_item['type'] == 'image' )
			return true;

		return false;
	}
}
