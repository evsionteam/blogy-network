(function($){

	function ajaxPost ( param ){
		this.page 		= 1;
		this.ajaxUrl	= BLOGGER.ajaxUrl;
		this.isBusy 	= false;
		this.loader     = param.loader;
		this.container  = param.container;
		this.timer      = 2000;
		this.run 		= function(){

			var that = this;

			$( this.loader ).hide();

			$( window ).scroll(function() {
		       if($(window).scrollTop() + $(window).height() - 200 == $(document).height() -200 ) {
		        	
					that.ajax();
		       }
		   });
		}

		this.ajax = function(){

			var that = this;
			var param = { 'cat_id' : BLOGGER.catId };

			if( typeof BLOGGER_CATEGORY !== 'undefined' ){
				param.category_name = BLOGGER_CATEGORY.category_name;
			}

			if( ! this.busy ){

				$( this.loader ).show();
				
				this.busy = true;

				param.page = ++this.page;
				
				jQuery.post(
				    this.ajaxUrl, 
				    {
				        'action' : 'get_paged_post',
				        'data'   :  param, 
				    }, 
				    function( response,status,xhr ){ that.display( response, status, xhr ); }
				);
			}
		}

		this.display = function( response, status, xhr ){

			var that = this;

			setTimeout( function(){

	    		that.busy = false;
		    	$( that.loader ).hide();

		    	if( status == 'success' ){

			    	if( response == 0 ){
			    		that.busy = true;
			    		console.info( 'Data finished' );
			    	}else{
		    			$( that.container ).append( response );
			    	}
		    	}
	    	}, that.timer);
		}
	}

	$( document ).ready( function(){
		var ap = new ajaxPost( { 'container' : '#ajax-post-container', 'loader' : '.post-loading' });
		ap.run();
	});

})(jQuery)