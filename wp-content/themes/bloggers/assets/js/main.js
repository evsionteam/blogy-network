(function( $ ){

	function likeit( params ){

		this.postId      	= null;
		this.userId      	= null;
		this.totalLikes  	= 'undefined';
		this.likeCounter 	= params.likeCounter
		this.button 		= params.btn;
		this.url			= params.url;
		this.action 		= params.action;
		this.busy   		= false;
		this.ele            = null;
		this.status         = 0;

		this.run = function(){

			var that = this;

			$( this.button ).on( 'click', function(e){
				e.preventDefault();
				that.ele = this;
				that.userId = $(this).attr( 'data-user' );
				that.postId = $(this).attr( 'data-post' );
				that.status = parseInt( $(this).attr( 'data-status' ) );
				that.totalLikes = parseInt( $( that.likeCounter, this).text() );

				if( that.userId == 0 || typeof that.userId == 'undefined' || that.status == 1 ){
					//Not Logged in.
					return;
				}

				if( !that.busy ){
					that.like();
				}
			});
		}

		this.like = function(){
			
			this.busy = true;

			this.increaseCounter();

			$.ajax({
				
				url	     : this.url,
				type     : 'POST',
				dataType : 'JSON',
				data     : {
					'post_id' : this.postId,
					'user_id' : this.userId,
					'action'  : this.action
				},
				success   : function( data, textStatus, jqXHR ){
					this.display( data, textStatus, jqXHR );
				},
				error     : function( xhr, status, error ){
					this.error( xhr, status, error );
				},
				complete  : function( xhr, status ){
					this.complete( xhr, status );
				},
				context : this
			});
		}

		this.display = function( data, textStatus, jqXHR ){
			if( data[ 'status' ] == "unsuccess" ){
				this.decreaseCounter();
				if( data['message'] = 'Liked Already.' ){

				}
			}else{

				$( this.ele ).attr( 'data-status', 1 ); 
			}
		}

		this.error = function( xhr, status, error ){
			this.decreaseCounter();
		}

		this.complete = function( xhr, status ){
			this.busy = false;
		}

		this.increaseCounter = function(){
			if( typeof this.totalLikes !== 'undefined' ){
				$( this.likeCounter, this.ele ).text( this.totalLikes + 1 );
			}

			$( this.ele ).find('.fa').removeClass( 'fa-heart-o' ).addClass( 'fa-heart' );
		}

		this.decreaseCounter = function(){
			if( typeof this.totalLikes !== 'undefined' ){
				$( this.likeCounter, this.ele ).text( this.totalLikes );
			}

			$( this.ele ).find('.fa').removeClass( 'fa-heart' ).addClass( 'fa-heart-o' );
		}
	}

	$( document ).ready( function(){

		
		var like = new likeit({
			'btn'         : '.like-it',
			'url'         : BLOGGER_GLOBALS.ajaxUrl,
			'action' 	  : 'like_post',
			'likeCounter' : '.like-count em'
		});

		like.run();

		$("a[rel^='prettyPhoto']").prettyPhoto({theme:'light_square'});
	
		
	});

})(jQuery)