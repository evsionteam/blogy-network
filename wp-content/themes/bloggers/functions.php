<?php
show_admin_bar( false );

add_filter('widget_text', 'do_shortcode');

if ( ! function_exists( 'blogger_setup' ) ) :

	function blogger_setup() {
		
		load_theme_textdomain( 'blogger', get_template_directory() . '/languages' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'blogger' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-background', apply_filters( 'blogger_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'post-formats', array(
			'video',
		) );
	}
endif;
add_action( 'after_setup_theme', 'blogger_setup' );

function blogger_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'blogger_content_width', 640 );
}
add_action( 'after_setup_theme', 'blogger_content_width', 0 );

function blogger_widgets_init() {

	register_sidebar( array(
			'name'          => esc_html__( 'Sidebar', 'blogger' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'blogger' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);

	register_sidebar( array(
			'name'			=> esc_html__( 'After Banner', 'blogger' ),
			'id' 			=> 'sidebar-2',
			'description'   => esc_html__( 'Add widgets here.', 'blogger' ),
			'before_widget' => '<div id="%1$s" class="horizontal-widget %2$s col-sm-4">',
			'after_widget'  => '</div>',
		) 
	);
}
add_action( 'widgets_init', 'blogger_widgets_init' );

function blogger_scripts() {

	
	$assets_url = get_stylesheet_directory_uri().'/assets';

	wp_enqueue_style( 'blogger-style', get_stylesheet_uri() );

	// wp_enqueue_script( 'blogger-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	// wp_enqueue_script( 'blogger-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_register_script( 'bootstrap', $assets_url.'/js/bootstrap.min.js', array( 'jquery' ), null, true );
	wp_register_script( 'menu', $assets_url.'/js/menu.js', array( 'bootstrap' ), null, true );

	wp_register_script( 'prettyphoto', $assets_url.'/js/jquery.prettyPhoto.js', array( 'menu' ), null, true );

	wp_enqueue_script( 'script',  $assets_url.'/js/main.js', array( 'prettyphoto' ), null, true );

	wp_localize_script( 'script', 'BLOGGER_GLOBALS', array(
			'ajaxUrl' => admin_url('admin-ajax.php')
		));

	if( is_front_page() || is_category() ){
		wp_enqueue_script( 'ajax-post-loader', $assets_url . '/js/ajax-post-loader.js', 
				array('jquery'), null, true );

		$obj = get_queried_object();

		$cat_id = $obj ? $obj->term_id : null;

		$js_data = array(
			'ajaxUrl'     => admin_url('admin-ajax.php'),
			'catId'       => $cat_id
			);

		wp_localize_script( 'ajax-post-loader', 'BLOGGER', $js_data );
	}
	
	$webfont = "WebFontConfig = {
			google: { families: [ 'PT+Sans:400,400italic,700,700italic:latin', 'Open+Sans:400,400italic,600,600italic,300,300italic,700,700italic,800:latin' ] }
		 };
		 (function() {
		   var wf = document.createElement('script');
		   wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		   wf.type = 'text/javascript';
		   wf.async = 'true';
		   var s = document.getElementsByTagName('script')[0];
		   s.parentNode.insertBefore(wf, s);
		 })(); ";

	wp_add_inline_script('script', $webfont );

	
}
add_action( 'wp_enqueue_scripts', 'blogger_scripts' );

function blogger_styles(){

	global $blogger_option;
	$assets_url = get_stylesheet_directory_uri().'/assets';

	wp_register_style( 'fa', $assets_url.'/css/font-awesome.min.css' );
	wp_register_style( 'bootstrap', $assets_url.'/css/bootstrap.min.css' );
	wp_register_style( 'menu', $assets_url.'/css/menu.css' );
	wp_register_style( 'pretty-photo', $assets_url.'/css/prettyPhoto.css' );

	$deps = array( 'bootstrap','fa', 'menu', 'pretty-photo' );
	wp_enqueue_style( 'bloggers-style', $assets_url.'/css/main.css', $deps );

	$jumbo_btn_text_color = isset( $blogger_option['jumbo_btn_text_color']['rgba'] ) ? $blogger_option['jumbo_btn_text_color']['rgba'] : 'rgba(180, 139, 139, 0)';

	$jumbo_btn_bg_color = isset( $blogger_option['jumbo_btn_bg_color']['rgba'] ) ? $blogger_option['jumbo_btn_bg_color']['rgba'] : '#b19a6f';

	$jumbo_btn_border_color = isset( $blogger_option['jumbo_btn_border_color']['rgba'] ) ? $blogger_option['jumbo_btn_border_color']['rgba'] : '#b19a6f';


	$inline_styles = "
						.banner-content h2 div{
							color: ".$jumbo_btn_text_color.";
							background-color: ".$jumbo_btn_bg_color .";
							border: 1px ". $jumbo_btn_border_color." solid;
						}

					";
	wp_add_inline_style('bloggers-style', $inline_styles );
}
add_action( 'wp_enqueue_scripts', 'blogger_styles' );

function admin_blogger_scripts() {

    wp_enqueue_media();
    wp_enqueue_script( 'admin', get_stylesheet_directory_uri() . '/admin/admin.js', array( 'jquery' ), null, true );
}

add_action('admin_enqueue_scripts', 'admin_blogger_scripts');

require get_template_directory() . '/admin/loader.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/jetpack.php'; 
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
require get_template_directory() . '/inc/widgets.php';
require get_template_directory() . '/inc/maid.php';
require get_template_directory() . '/inc/shortcode.php';

function add_meta_box_post_video() {
    add_meta_box( 'post_video', __( 'Video Link', 'blogger' ), 'populate_video_link', 'post' );
}
add_action( 'add_meta_boxes', 'add_meta_box_post_video' );

function populate_video_link( $post ){

	wp_nonce_field( 'post_video_nonce_value', 'post_video_nonce_field' );
 
    $value = get_post_meta( $post->ID, 'video_link', true );
?>
	<input type="text" 
		name="video_link" 
		value="<?php echo $value; ?>" 
		style="width: 100%;" 
		placeholder="<?php _e( 'Link for Video','blogger' ) ?>" />
<?php
}

function blogger_save_meta_box( $post_id ) {

	if( !isset( $_POST['video_link']) )
		return $post_id;	

    $video_link = sanitize_text_field( $_POST['video_link'] );
 	
 	$nonce = $_POST['post_video_nonce_field'];
 
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $nonce, 'post_video_nonce_value' ) ) {
        return $post_id;
    }

    // Update the meta field.
    update_post_meta( $post_id, 'video_link', $video_link );
}
add_action( 'save_post', 'blogger_save_meta_box' );

add_action( 'wp_ajax_get_paged_post', 'blogger_ajax_get_paged_posts' );
add_action( 'wp_ajax_nopriv_get_paged_post', 'blogger_ajax_get_paged_posts' );

function blogger_ajax_get_paged_posts() {

    $data = ( !isset( $_POST[ 'data' ]) ) ? 1 : $_POST[ 'data' ];

    $page = $data['page'];
    global $withcomments;

	$withcomments = true;

    $args = array( 'paged' => $page );

    if( isset( $data['cat_id'] ) ){
    	$args['cat'] = $data['cat_id'];
    }

    if( isset( $data['category_name']) ){
    	$args['category_name'] = $data['category_name'];
    }

    $query = new WP_Query( $args );

    if( $query->have_posts() ):
    	while ( $query->have_posts() ) : 
    		echo "<div class='post-wrapped'>";
    		$query->the_post();
    	
			get_template_part( 'template-parts/content' );

			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			?>

			<?php
			echo '</div>';			
    	endwhile;
    	wp_reset_postdata();
    endif;

    die();
}

//add_filter( 'term_link', 'blogger_modify_term_url' ,10,2 );

function blogger_modify_term_url($termlink, $term){
	return site_url().'/'.$term->slug;
}

function blogger_filter_categories( $query ) {
	global $blogger_option;
	
	if( is_admin() && !is_network_admin() ):
		$meta_query = $query->get('meta_query');	
		$meta_query['relation'] = 'AND';
		$meta_query[] = array(
                    'key'=>'hide_page_in_admin',
                    'value'=>'on',
                    'compare'=>'NOT EXISTS',
                );
		
		$query->set('meta_query',$meta_query);
    endif;
}
//add_action( 'pre_get_posts', 'blogger_filter_categories' );

function blognetwork_search_form( $form ) {

    $form  = '';
    $form .= '<form role="search" method="get" id="searchform" class="search-form" action="' . home_url( '/' ) . '" >';
    $form  .= '<span class="screen-reader-text">' . _x( 'Search for:', 'blognetwork' ) . '</span>';
        $form .= '<input type="search" class="search-field" name="s" placeholder="' . __('type keyword(s) here','blognetwork') . '" />';
        $form .= '<button type="search id="" class="btn btn-primary search-submit">'. esc_attr__( 'Search' ) .'</button>';
    $form .= '</form>';
 
    return $form;
}
add_filter( 'get_search_form', 'blognetwork_search_form' ) ;

add_action ('admin_init','remove_user_capabilities');
function remove_user_capabilities(){
	$role = get_role( 'administrator' );
	$admin_stripped_capabilities = array(
        'edit_dashboard',
        'switch_themes',
        'edit_themes',
        'install_themes',
        'delete_themes',
        'activate_plugins',
        'edit_plugins',
        'install_plugins',
        'update_core',
        'edit_users',
        'list_users',
        'remove_users',
        'add_users',
        'promote_users',
        'manage_sites',
        'manage_categories',
        'manage_options'
    );
	
	foreach ( $admin_stripped_capabilities as $cap ) {
		//$role->remove_cap( $cap );
    	$role->add_cap( $cap);
    }
}

function restrict_menus(){

	$user = wp_get_current_user();
	$screen = get_current_screen();
    $base = $screen->id;
  
	if(!isset($user)){ 
 	   return;
	}

	$restricted_menus = array(
		'themes',
		'customize',
		'nav-menus',
		'tools',
		'import',
		'export',
		'ms-delete-site',
	);
	
    if( in_array($base, $restricted_menus) ){
        wp_die('Cheatin’ uh?');
    }
}

add_action( 'current_screen', 'restrict_menus' );

function alter_admin_menus(){

	$user = wp_get_current_user();
	
	if(!isset($user)){ 
 	   return;
	}

	remove_menu_page( 'themes.php' );
	remove_submenu_page( 'tools.php','import.php' );
	remove_submenu_page( 'tools.php','export.php' );
	remove_submenu_page( 'tools.php','ms-delete-site.php' );
	remove_menu_page( 'tools.php' );
	
	add_menu_page(
        __( 'Widgets'),
        'Widgets',
        'manage_options',
        '/widgets.php',
        '',
        'dashicons-tagcloud',
        25
    );
}
add_action( 'admin_menu', 'alter_admin_menus' );

add_action( 'wp_ajax_like_post', 'blogger_ajax_like_post' );
add_action( 'wp_ajax_nopriv_like_post', 'blogger_ajax_like_post' );

function blogger_ajax_like_post() {

    $post_id = $_POST['post_id'];
    $user_id = $_POST['user_id'];

    $return = array(
    	'status' => 'unsuccess',
    	'data'	 => null,
    	'message' => null
    	);

    if( $post_id && $user_id ){

    	$likes = get_post_meta( $post_id, 'post_likes',true );

    	if( $likes ){

    		if( !in_array( $user_id, $likes ) ){
    			
    			array_push( $likes,$user_id  );
    			update_post_meta( $post_id, 'post_likes', $likes );
    			$return['status'] = 'success';
    		}else{
    			$return['message'] = 'Liked Already.';
    		}
    	}else{

    		$likes = array( $user_id );
    		update_post_meta( $post_id, 'post_likes', $likes );
    		$return['status'] = 'success';
    	}
    }

    echo json_encode($return);
    die();
}