<?php global $blogger_option; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info">
				<a href="#"><?php echo $blogger_option['footer_text']; ?></a>
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
</div>  <!-- page -->
<?php wp_footer(); ?>
	</body>
</html>
