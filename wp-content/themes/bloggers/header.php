<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<?php
	$user_id =  get_option( 'main_user' );
	$blog_user = get_user_by( 'ID', $user_id );
	global $bn_opt, $post;
	
	$terms = get_terms( array( 'taxonomy' => 'category', 'exclude' => 1 )); 
?>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<header id="masthead" class="site-header header-main" role="banner">
				<div class="container">
					<div class="page-wrapper">
						<div class="row">
							<div class="col-sm-11">
								<h1>
									<a href="<?php echo site_url(); ?>"><?php echo $blog_user->display_name; ?></a>
								</h1>
								<ul class="tags">
									<?php
										if( is_array($terms) ):
											$count = count( $terms );
											foreach( $terms as $key => $t ): 
									?>
											<li>
												<a href="<?php echo get_term_link ( $t ); ?>">
													<?php echo $t->name; ?>
												</a>
												<?php if( $key < $count-1 ): ?>
													<span class="fa fa-circle"></span>
												<?php endif; ?>
											</li>
									<?php
											endforeach; 
										endif; 
									?>
								</ul>
							</div>
							<div class="col-sm-1 hidden">
								<div class="nav-menu"> 
									<button class="button-nav-toggle"><span class="icon">&#9776;</span></button>
								</div>
							</div>
						</div>
					</div>
				</div><!-- container -->	
			</header><!-- site-header -->

			<div class="menu">
			  <nav class="nav-main">
			    <div class="nav-container">
			    <?php 
			    	if( class_exists( 'wp_bootstrap_navwalker' ) ):
				    	$args = array(
				    		'menu' => 'primary',
				    		'menu_class' => '',
				    		'walker' => new wp_bootstrap_navwalker
				    	);
				    	//wp_nav_menu ( $args  );
			    	endif;
			    ?>
			     <!--  <ul>
			        <li><a href="">About Us</a></li>
			        <li><a href="">Services</a></li>
			        <li> <a href="#">Careers</a>
			          <ul>
			            <li><a href="#" class="back">Main Menu</a></li>
			            <li class="nav-label"><strong>JOBS IN AMERICA</strong></li>
			            <li><a href="">Accountant</a></li>
			            <li><a href="">Budget Analyst</a></li>
			            <li><a href="">Carpenter</a></li>
			            <li><a href="">Desktop Publisher</a></li>
			            <li><a href="">Electrical Engineer</a></li>
			            <li><a href="">Financial Analyst</a></li>
			            <li><a href="">Human Resources Specialists</a></li>
			            <li><a href="">Loan Officer</a></li>
			            <li><a href="">Reporter</a></li>
			            <li class="nav-label"><strong>JOBS IN GERMANY</strong></li>
			            <li><a href="">Budget Analyst</a></li>
			            <li><a href="">Carpenter</a></li>
			            <li><a href="">Electrical Engineer</a></li>
			            <li><a href="">Financial Analyst</a></li>
			            <li><a href="">Human Resources Specialists</a></li>
			            <li><a href="">Loan Officer</a></li>
			          </ul>
			        </li>
			        <li><a href="">Downloads</a></li>
			        <li><a href="">Contact</a></li>
			      </ul> -->
			    </div>
			  </nav>
			</div><!-- menu -->
