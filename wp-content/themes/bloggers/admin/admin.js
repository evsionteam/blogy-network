
(function( $ ){

	var videoLinkToggler = function(){
		var $video_meta = $( '#post_video' );
			
		if( $( "#post-format-video:checked" ).length == 0 ){
			$video_meta.slideUp();
		}

		$( '.post-format, .post-format-icon' )
			.on( 'click', function(){
				
				if( $( "#post-format-video:checked" ).length == 1 ){
					$video_meta.slideDown();
				}else{
					$video_meta.slideUp();
				}
		});
	}

	function media_upload(button_class) {

        $('body').on('click', button_class, function(e) {
           
            var target = $(this).attr('data-target');
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = $('#'+$(this).attr('id'));

            wp.media.editor.send.attachment = function(props, attachment){
                $(target).val(attachment.url);
            }
            wp.media.editor.open(button);
                return false;
        });
    }

	$( document )
		.ready( function(){
			videoLinkToggler();
			media_upload('.custom_media_button');
			
	});

})( jQuery )