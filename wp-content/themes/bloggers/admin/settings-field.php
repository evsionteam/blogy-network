<?php
    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    $blogger_option = 'blogger_option'; 
    $args = array( 
                'menu_title' => 'Bloggers Options', 
                'page_title' => 'Bloggers Options', 
                'save_defaults' => 'true'
                
            );
    Redux::setArgs ($blogger_option, $args);
    /* General Settings */

    $fields = array(
            array(
                'id'       => 'header_image',
                'type'     => 'media',
                'title'    => __( 'Header Image', 'blogger' ),
                'default'  => ''
                
            ),
            array(
                'id'       => 'jumbo_btn_text',
                'type'     => 'text',
                'title'    => __( 'Jumbo Button Text', 'blogger' ),
                'default'  => 'Welcome'
            ),
            array(
                'id'            => 'jumbo_btn_bg_color',
                'type'          => 'color_rgba',
                'title'         => 'Jumbo Button Bg Color',
                'default'       => array(
                    'color'     => '#fff',
                    'alpha'     => 1
                ),
                'permissions' => 'manage_network'
            ),
            array(
                'id'            => 'jumbo_btn_text_color',
                'type'          => 'color_rgba',
                'title'         => 'Jumbo Button Text Color',
                'default'       => array(
                    'color'     => '#b19a6f',
                    'alpha'     => 1
                ),
                'permissions' => 'manage_network'
            ),
            array(
                'id'            => 'jumbo_btn_border_color',
                'type'          => 'color_rgba',
                'title'         => 'Jumbo Button Border Color',
                'default'       => array(
                    'color'     => '#b19a6f',
                    'alpha'     => 1
                ),
                'permissions' => 'manage_network'
            ),
            array(
                'id'       => '404_title',
                'type'     => 'text',
                'title'    => __( '404 Title', 'blogger' ),
                'default'  => 'Hittades inte, Fel 404',
                'permissions' => 'manage_network'
            ),
            array(
                'id'       => '404_desc',
                'type'     => 'textarea',
                'title'    => __( '404 Description', 'blogger' ),
                'default'  => 'Sidan du söker finns inte längre. Kanske kan du gå tillbaka till webbplatsens startsida och se om du kan hitta vad du letar efter.',
                'permissions' => 'manage_network'
            )
        );

    Redux::setSection( $blogger_option, array(
        'title' => __( 'General Settings', 'blogger' ),
        'id'    => 'general',
        'icon'  => 'el el-dashboard',
        'fields'     => $fields,
        
    ) );

    Redux::setSection( $blogger_option, array(
        'title' => __( 'Footer Settings', 'blognetwork' ),
        'id'    => 'footer',
        'icon'  => '',
        'permissions' => 'manage_network',
        'fields'     => array(
            array(
                'id'        => 'footer_text',
                'type'      => 'text',
                'title'     => __( 'Footer Text', 'blognetwork' ),
                'default'   => __( 'Copyright &copy; 2016 Eaglevision IT SE', 'blognetwork' ),
            )
        )
    ) );


