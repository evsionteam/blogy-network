<?php get_header(); ?>
<div id="content" class="site-content">
	<div class="container">
		<?php get_template_part( 'template-parts/content','banner') ?>
		<div class="blog-content-wrap">
		    <div class="row">
		    	<div class="col-sm-9">
		    		<?php the_post(); ?>
		    		<div>
    					<div class="blog-post-list">
							<div class="blog-content-block">
								<article>
									<div class="post-title-wrap">
										<h2 class="post-title">
											<a href="<?php the_permalink(); ?>">
											<?php the_title(); ?></a>
										</h2>
										<div class="post-info"><span><?php the_date(); ?></span></div>
									</div>
									<?php if( has_post_thumbnail() ): ?>
										<a href="<?php the_permalink(); ?>">
											<div class="post-thumbnail" style="background-image:url('<?php the_post_thumbnail_url();?>')">
											</div>
										</a>
									<?php endif; ?>
									<div class="post-content-detail">
										<?php the_content();  ?>
									</div>
								</article>
							</div>
						</div><!-- blog-post-list -->
			    	</div>
		    	</div>
		    	<div class="col-sm-3">
		    		<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
						<?php dynamic_sidebar( 'sidebar-1' ); ?>
					<?php endif; ?>
		    	</div>
		    </div>
		    
		</div><!-- blog-content-wrap -->
	</div><!-- /container -->
</div><!-- site-content -->	
<?php get_footer(); ?>
