<?php get_header(); global $bn_opt; ?>
<div class="container">
	<div class="error 404 error-page">
		<h2 class="">
			<span class="text-frst">4</span>
			<span class="text-sec">0</span>
			<span class="text-thrd">4</span>
		</h2>
		<h3 class=""> <?php echo $blogger_option['404_title']; ?></h3>
		<article><?php echo $blogger_option['404_desc']; ?></article>
   </div>
</div>

<?php get_footer();
