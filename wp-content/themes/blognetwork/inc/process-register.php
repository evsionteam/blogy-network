<?php

require_once ABSPATH . 'wp-admin/includes/ms.php'; 
function process_register(){

	$nonce_name   = isset( $_POST['registering_blogger_nonce'] ) ? $_POST['registering_blogger_nonce'] : '';
    $nonce_action = 'registering_blogger';

    // Check if nonce is set.
    if ( ! isset( $nonce_name ) ) {
        return;
    }

    // Check if nonce is valid.
    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
        return;
    }

	if( isset( $_POST['blogger_registration'] )){
		
		$blogger_categories 	= isset( $_POST['blogger_categories'] ) ? $_POST['blogger_categories'] : array();
		
		$user_login 	     	= $_POST['user_name'];
		$user_password  	 	= $_POST['password'];
		$email     	        	= $_POST['email'];
		$first_name         	= $_POST['first_name'];
		$last_name          	= $_POST['last_name'];
		$remember				= true;

		if( empty( $user_login ) || empty( $user_password ) || empty($email) || empty( $first_name ) || empty( $last_name ) ){
			set_transient( 'validation_error', array( 'Some of the fields cannot be empty.' ) );
			return;
		}

		$creds = compact("user_login","user_password","remember");
		$args = array(
			'first_name' => $first_name,
			'last_name'  => $last_name,
			'user_login' => $user_login,
			'user_email' => $email,
			'user_pass'  => $user_password,
			'role'		 => 'administrator'
		);

		$user_id  = wp_insert_user($args);
		
		if( is_wp_error( $user_id) ){
			set_transient( 'blog_notice', $user_id );
			return;
		}

		if( $user_id  ){
			//User created Successfully

			update_user_meta( $user_id, 'first_name', $first_name );
			update_user_meta( $user_id, 'last_name', $last_name );

			$title  = $first_name . ' ' . $last_name;
			$domain =  network_site_url();
			$domain =  DOMAIN_CURRENT_SITE;
			
			$path   = PATH_CURRENT_SITE.$user_login.'/';

		    $active_widgets = array (
				'wp_inactive_widgets' => array ('archives','meta','search','categories','recent-posts','recent-comments'),
		        'sidebar-1' => array(
		        	'my_profile_widget-1',
		            'follow_me_widget-2',
		            'blogger-instagram-feed-3',
		            'categories-4'
		        ),
		        'sidebar-2' => array(
		        	'thumbnail_category_widget-1',
		            'thumbnail_page_widget-2',
		            'thumbnail_category_widget-3'
		        ),
		        'array_version' => 3
		    );

		    //Setting default value for widgets.

		    $widget_my_profile_widget = array (
				1 => array (
					   'title' => $first_name,
					   'description' => '',
					   'image_uri'   => 'https://placeholdit.imgix.net/~text?txtsize=32&txt=Profile&w=200&h=200&txttrack=0'
					 ),
				'_multiwidget' => 1,
			);
			
	    	$widget_follow_me_widget = array (
				2 => array (
					   'title' => 'Social',
					   'fb' => 'https://facebook.com',
					   'twitter' => 'https://twitter.com',
					   'google' => 'https://google.com',
					   'instagram' => 'https://instagram.com',
					   'behance' => 'https://behance.net',
					   'youtube' => 'http://youtube.com',
					 ),
				'_multiwidget' => 1,
			);

	    	$widget_thumbnail_category_widget = array(
	    		1 => array(
	    			'term_id' => 1,
	    			'image_uri' => 'https://placeholdit.imgix.net/~text?txtsize=32&txt=Category&w=300&h=300&txttrack=0'
	    			),
	    		3 => array(
	    			'term_id' => 1,
	    			'image_uri' => 'https://placeholdit.imgix.net/~text?txtsize=32&txt=Category&w=300&h=300&txttrack=0'
	    			)
	    		);

	    	$widget_thumbnail_page_widget = array(
	    		2 => array(
	    			'page_id' => 2,
	    			'image_uri' => 'https://placeholdit.imgix.net/~text?txtsize=32&txt=Page&w=300&h=300&txttrack=0'
	    			)
	    		);

			$blog_id = wpmu_create_blog($domain, $path, $title, $user_id, array( 
				'template'   				       => 'bloggers',
				'stylesheet' 				       => 'bloggers',
				'main_user'  				       => $user_id,
				'sidebars_widgets' 			       => $active_widgets,
				'widget_follow_me_widget' 		   => $widget_follow_me_widget,
				'widget_my_profile_widget' 		   => $widget_my_profile_widget,
				'widget_thumbnail_category_widget' => $widget_thumbnail_category_widget,
				'widget_thumbnail_page_widget'	   => $widget_thumbnail_page_widget,
				'widget_blogger-instagram-feed'    => array(
						3 => array(
								'title'    => 'Instagram User',
								'username' =>'username',
								'number'   => 9,
								'size'	   => 'thumbnail'
							)
					),
				'widget_categories'		 			=> array(
						4 => array(
								'title'        => 'Categories',
								'count' 	   => 'on',
								'hierarchical' => 'on'
							)
					)
			) );

			if( $blog_id ){

				add_user_to_blog( $blog_id, NETWORK_ADMIN_ID , 'administrator' );
				grant_super_admin( $blog_id );

				switch_to_blog($blog_id);
				//Adding Post Categories 
				if( is_array( $blogger_categories ) ){
					foreach( $blogger_categories as $cat ){
						wp_insert_term( $cat, 'category' );
					}
				}
				
				save_disqus_details();

				remove_user_from_blog($user_id, 1);
				$user = wp_signon( $creds, false );
				
				if(!is_wp_error($user)){
					wp_redirect( get_admin_url( $blog_id ) );
					exit();
				}
			}

		}else{
			//error on User creation
		}
	}
}

add_action( 'init', 'process_register' );