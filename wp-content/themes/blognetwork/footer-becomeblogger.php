<?php global $bn_opt; ?>
<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<h1 class="site-title">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<?php if( isset( $bn_opt[ 'footer_logo' ][ 'url' ] ) ):  ?>
					 <img src="<?php echo $bn_opt[ 'footer_logo' ][ 'url' ]; ?>" >
				<?php endif; ?>	
				<!-- <img src="<?php echo get_template_directory_uri() .'/assets/images/footer-logo.png'; ?>"> -->
			</a>
			</h1>
			   <p><?php echo $bn_opt[ 'footer_text']; ?></p>
			   <div class="social-links">
			   		<ul>
				   		<?php if( isset( $bn_opt[ 'twitter_link' ] ) && !empty( $bn_opt['twitter_link']) ): ?>
				   			<li><a href="<?php echo $bn_opt[ 'twitter_link' ]; ?>"><i class="fa fa-twitter"></i></a></li>
				   		<?php endif; ?>

				   		<?php if( isset( $bn_opt[ 'pinintrest_link' ] ) && !empty( $bn_opt['pinintrest_link']) ): ?>	
				   			<li><a href="<?php echo $bn_opt[ 'pinintrest_link' ]; ?>"><i class="fa fa-pinterest"></i></a></li>
				   		<?php endif; ?>

				   		<?php if( isset( $bn_opt[ 'fb_link' ] ) && !empty( $bn_opt['fb_link']) ): ?>	
				   			<li><a href="<?php echo $bn_opt[ 'fb_link' ]; ?>"><i class="fa fa-facebook"></i></a></li>
				   		<?php endif; ?>

				   		<?php if( isset( $bn_opt[ 'instagram_link' ] ) && !empty( $bn_opt['instagram_link']) ): ?>	
				   			<li><a href="<?php echo $bn_opt[ 'instagram_link' ]; ?>"><i class="fa fa-instagram"></i></a></li>
				   		<?php endif; ?>

				   		<?php if( isset( $bn_opt[ 'skype_link' ] ) && !empty( $bn_opt['skype_link']) ): ?>	
				   			<li><a href="<?php echo $bn_opt[ 'skype_link' ]; ?>"><i class="fa fa-skype"></i></a></li>
				   		<?php endif; ?>

				   		<?php if( isset( $bn_opt[ 'dribble_link' ] ) && !empty( $bn_opt['dribble_link']) ): ?>	
				   			<li><a href="<?php echo $bn_opt[ 'dribble_link' ]; ?>"><i class="fa fa-dribbble"></i></a></li>
				   		<?php endif; ?>
			   		</ul>
			   </div>
			
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->
	
	<?php wp_footer(); ?>

</body>
</html>