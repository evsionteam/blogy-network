<!DOCTYPE html>
<html  <?php language_attributes(); ?>>
<head>
	<title>BLOGY</title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>

<?php
	global $bn_opt;
?>
<body <?php body_class(); ?>>
	
<!----- HEADER STARTS ------>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main">
	<?php esc_html_e( 'Skip to content', 'underscore-theme' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<nav class="main-navigation navbar navbar-default navbar-fixed-top">
            
            <!--begin container -->
            <div class="container">
            	<div class="nav-wrap clearfix">
	        
	                <!--begin navbar -->
	                <div class="col-md-2 col-xs-4">
		                <div class="navbar-header">
		             
	                                                    
							<h1 class="site-title">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
								<?php if( isset( $bn_opt[ 'logo' ][ 'url' ] ) ):  ?>
									<img src="<?php echo $bn_opt[ 'logo' ][ 'url' ]; ?>" class="img-responsive">
								<?php endif; ?>	
								</a>
							</h1>
		                </div><!-- /.navbar-header -->
	                </div><!-- /.col-md-3 -->

					<div class="col-xs-12 col-md-7" id="menu">
		        		<div class="navbar-collapse ">
							<?php 
								$args = array(
					    		'menu' => 'primary',
					    		'menu_class' => 'nav navbar-nav list-inline'
					    	);
				    		wp_nav_menu ( $args  ); ?>
						</div> <!-- /.navbar-collapse -->
					</div><!-- /.col-md-9 -->

	                <!--end navbar -->
	                <div class="col-md-3 col-sm-8">

						<button data-target="#navbar-collapse-02" class="navbar-toggle collapsed pull-right" type="button">
	                        <span class="sr-only">Toggle navigation</span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                    </button>
                		<div class="blogger-btn pull-right">
							<a class="site-btn" href="<?php echo $bn_opt['head_btn_link']; ?>"><?php echo $bn_opt['head_btn_text']; ?></a>
						</div>
					</div><!-- /.col-md-3 -->

				</div><!-- /.nav-wrap -->
                                    
            </div><!--end container -->
            
        </nav>
	
	</header><!-- #masthead -->