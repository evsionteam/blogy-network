<?php get_header(); ?>
<div id="content" class="site-content">
	<div class="container">
  		<?php get_template_part( 'template-parts/content','banner') ?>
		<div class="blog-content-wrap">
		    <div class="row">
		    	<div class="col-sm-9 search-results">
		    		<?php
						if ( have_posts() ) : ?>

							<header class="page-header">
								<h1 class="page-title">
									<?php printf( esc_html__( 'Search Results for: %s', 'blognetwork' ), '<span>' . get_search_query() . '</span>' ); ?>
									
								</h1>
							</header><!-- .page-header -->
							<?php
							/* Start the Loop */
							while ( have_posts() ) : the_post();
								get_template_part( 'template-parts/content', 'search' );
							endwhile;
							the_posts_navigation();
						else :
							get_template_part( 'template-parts/content', 'none' );

						endif; ?>
		    	</div>

		    	<div class="col-sm-3">
		    		<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
						<?php dynamic_sidebar( 'sidebar-1' ); ?>
					<?php endif; ?>
		    	</div>
		    </div>
		</div><!-- blog-content-wrap -->
	</div><!-- /container -->
</div><!-- site-content -->		
<?php get_footer();
