<?php get_header(); ?>
<div id="content" class="site-content">
	<div class="container">
  		<?php get_template_part( 'template-parts/content','banner') ?>
		<div class="blog-content-wrap">
		    <div class="row">
		    	<div class="col-sm-9">
		    		<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post();  ?>
						<?php 
							get_template_part( 'template-parts/content' );

							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>
					<?php endwhile;  ?>
					<?php else : ?>
						<?php get_template_part( 'template-parts/content', 'none' ); ?>
					<?php endif; ?>
		    	</div>

		    	<div class="col-sm-3">
		    		<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
						<?php dynamic_sidebar( 'sidebar-1' ); ?>
					<?php endif; ?>
		    	</div>
		    </div>
		    
		</div><!-- blog-content-wrap -->
	</div><!-- /container -->
</div><!-- site-content -->	
<?php get_footer(); ?>