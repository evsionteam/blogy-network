<?php

show_admin_bar(false);
require_once 'admin/loader.php';

if ( ! function_exists( 'blognetwork_setup' ) ) :

function blognetwork_setup() {

	load_theme_textdomain( 'blognetwork', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'blognetwork' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'blognetwork_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'blognetwork_setup' );

function blognetwork_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'blognetwork_content_width', 640 );
}
add_action( 'after_setup_theme', 'blognetwork_content_width', 0 );

function blognetwork_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'blognetwork' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'blognetwork' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'blognetwork_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function blognetwork_scripts() {

	$assets_url = get_stylesheet_directory_uri().'/assets';

	wp_enqueue_style( 'blognetwork-style', get_stylesheet_uri() );

	wp_register_style( 'fa', $assets_url.'/css/font-awesome.min.css' );
	wp_register_style( 'bootstrap', $assets_url.'/css/bootstrap.min.css' );
	wp_register_style( 'menu', $assets_url.'/css/menu.css' );
	wp_register_style( 'animate', $assets_url.'/css/animate.css' );

	wp_register_script( 'bootstrap', $assets_url . '/js/bootstrap.min.js', array( 'jquery'), null, true );
	wp_register_script( 'wow', $assets_url . '/js/wow.min.js', array( 'jquery'), null, true );
	if ( is_page_template( 'page-templates/template-become-a-member.php' ) ) {

		$deps = array( 'bootstrap','fa','animate');
		wp_enqueue_style( 'become-blogger-style', $assets_url.'/css/style.css', $deps );
		
			
		$webfont = "WebFontConfig = {
				    google: { families: [ 'Lato:400,300,300italic,400italic,700,700italic,900' ] }
				  };
				  (function() {
				    var wf = document.createElement('script');
				    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				    wf.type = 'text/javascript';
				    wf.async = 'true';
				    var s = document.getElementsByTagName('script')[0];
				    s.parentNode.insertBefore(wf, s);
				  })();";

		wp_enqueue_script( 'main', $assets_url . '/js/main.js', array( 'jquery','bootstrap', 'wow' ), null, true );
		wp_add_inline_script('main', $webfont );

	}else{

			$deps = array('bootstrap','fa','menu');

		wp_enqueue_style( 'bloggers-network-style', $assets_url.'/css/main.css', $deps );

		wp_enqueue_script( 'menu', $assets_url . '/js/menu.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'script', $assets_url . '/js/main.js', array( 'menu' ), null, true );

		$webfont = "WebFontConfig = {
			   google: { families: [ 'PT+Sans:400,400italic,700,700italic:latin', 'Open+Sans:400,400italic,600,600italic,300,300italic,700,700italic,800:latin' ] }
			 };
			 (function() {
			   var wf = document.createElement('script');
			   wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			   wf.type = 'text/javascript';
			   wf.async = 'true';
			   var s = document.getElementsByTagName('script')[0];
			   s.parentNode.insertBefore(wf, s);
			 })(); ";

		wp_add_inline_script('script', $webfont );
	}

}
add_action( 'wp_enqueue_scripts', 'blognetwork_scripts' );

//Processing Registeration Form
require get_template_directory() . '/inc/widgets.php';
require get_template_directory() . '/inc/process-register.php';

require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

require get_template_directory() . '/inc/helpers.php';

add_action( 'edit_category' , 'update_terms_in_all_blogs' ); 
function update_terms_in_all_blogs($term_id ){
   
	if( isset( $_POST) && $_POST['taxonomy'] == 'category' ){

	    $name        = $_POST['name'];
	    $slug        = $_POST['slug'];
	    $parent      = $_POST['parent'];
	    $description = $_POST['description']; 
	    
	    $term = get_term_by( 'id', $term_id, 'category' );
		$blogs = wp_get_sites();

		if( is_array( $blogs ) ){
			foreach( $blogs as $b ){

				if( $b['blog_id'] == 1 ){ continue; }

	    		switch_to_blog($b['blog_id']);

	    		$t = get_term_by( 'slug', $term->slug, 'category' );

	    		if( $t ){
	    			wp_update_term( $t->term_id, 'category', array(
					  'name'     	=> $name,
					  'slug'     	=> $slug,
					  'parent'      => $parent,
					  'description' => $description
					));
	    		}
	    	}
		}
	}

	return;
}

add_action( 'created_term', 'add_term_to_blog' );
function add_term_to_blog( $term_id ){

	$term = get_term_by( 'id', $term_id, 'category' );
	$blogs = wp_get_sites();
	
	if( $term && is_array( $blogs ) ):

		foreach( $blogs as $b ){

			if( $b['blog_id'] == 1 ){ continue; }

			switch_to_blog($b['blog_id']);

			$blog_term = get_term_by( 'slug', $term->slug, 'category' );
			if( !$blog_term ){
				$t = wp_insert_term( $term->name, $term->taxonomy, $args = array(
					'description' => $term->description,
					'parent'	  => $term->parent,
					'slug'	      => $term->slug	
					));
			}
		}

		switch_to_blog(1);
		
	endif; 
}

add_action( 'delete_category', 'delete_term_from_blog',10, 4 );
function delete_term_from_blog( $term_id, $tt_id, $deleted_term, $object_ids ){
	
	$blogs = wp_get_sites();
	
	if( is_array( $blogs ) ){
		
		foreach( $blogs as $b ){

			if( $b['blog_id'] == 1 ){ continue; }

			switch_to_blog($b['blog_id']);

			$blog_term = get_term_by( 'slug', $deleted_term->slug, 'category' );
			
			if( $blog_term ){
				$t = wp_delete_term( $blog_term->term_id, 'category' );
			}
		}

		switch_to_blog(1);
	}
}
