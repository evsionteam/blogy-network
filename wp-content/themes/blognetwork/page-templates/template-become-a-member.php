<?php
/* 
	Template Name: Become a Member
*/
	get_header( 'becomeblogger' );
?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">

			<section id="fullwid-banner" style="background-image: url(<?php echo $bn_opt['banner_bg']['url']; ?>)">

				<div class="container">

					<div class="banner-content">

						<div class="row">
							<div class="col-md-6">
								<div class="banner-text-wrap">
									<h2 class="wow fadeIn" data-wow-delay="0.5s">
										<?php echo $bn_opt['banner_title']; ?>
									</h2>
									<p class="wow fadeIn" data-wow-delay="1s">
										<?php echo $bn_opt['banner_desc']; ?>
									</p>
									<a class="join-btn site-btn" href="<?php echo $bn_opt['banner_btn_link']; ?>">
										<?php echo $bn_opt['banner_btn_text']; ?>
									</a>
								</div>
							</div>
							<div class="col-md-6">
								<div class="banner-image-wrap wow slideInRight" data-wow-delay="2s">
									<?php echo $bn_opt['banner_video']; ?>
								</div>
							</div>
						</div>

					</div><!-- .banner-content -->

				</div><!-- .container -->

			</section><!-- #fullwid-banner -->

			<section id="features">
				<div class="container">
					<div class="section-title-wrap">
						<h2><?php echo $bn_opt['why_us_title']; ?></h2>
						<div class="seperator"><i class="fa fa-star"></i></div>
						<p class="sub-title">
							<?php echo $bn_opt['why_us_desc']; ?>
						</p>
					</div>
					
					<div class="row">
						<div class="col-sm-4">
							<div class="feature-item">
								<div class="icon-box"><i class="<?php echo $bn_opt[ 'why_us_box_1_icon' ]; ?>"></i></div>
								<h3><?php echo $bn_opt[ 'why_us_box_1_title' ]; ?></h3>
								<p class="sub-title">
									<?php echo $bn_opt[ 'why_us_box_1_desc' ]; ?>
								</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="feature-item">
								<div class="icon-box"><i class="<?php echo $bn_opt[ 'why_us_box_2_icon' ]; ?>"></i></div>
								<h3><?php echo $bn_opt[ 'why_us_box_2_title' ]; ?></h3>
								<p class="sub-title"><?php echo $bn_opt[ 'why_us_box_2_desc' ]; ?></p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="feature-item">
								<div class="icon-box"><i class="<?php echo $bn_opt[ 'why_us_box_3_icon' ]; ?>"></i></div>
								<h3><?php echo $bn_opt[ 'why_us_box_3_title' ]; ?></h3>
								<p><?php echo $bn_opt[ 'why_us_box_3_desc' ]; ?></p>
							</div>
						</div>
					</div>
				</div>
			</section><!-- #features -->
			
			<section id="image-section" class="module parallax parallax-2">
		        <div class="container">
		        	<div class="image-section-inside">
				          <h2><?php echo $bn_opt[ 'cta_title' ]; ?></h2>
				          <p class="sub-title"><?php echo $bn_opt[ 'cta_sub_title' ]; ?></p>
				          <a  class="site-btn" href="<?php echo $bn_opt[ 'cta_btn_link' ]; ?>" type="button">
				          	<?php echo $bn_opt[ 'cta_btn_title' ]; ?>
				          </a>
		            </div>
		        </div>
		    </section><!-- #image-section -->

		    <section id="contact">
		    	<div class="container">

		    		<div class="section-title-wrap">
						<h2><?php echo $bn_opt[ 'contact_title' ]; ?></h2>
						<div class="seperator"><i class="fa fa-star"></i></div>
						<p class="sub-title"><?php echo $bn_opt[ 'contact_desc' ]; ?></p>
					</div>

		    		<div class="row">
		    		<div class="col-md-offset-2 col-md-8">
			    			<div class="row">
			    			<?php  echo do_shortcode('[contact-form-7 id="49" title="Contact form 1" html_id="contact-form" html_class="contact-form"]'); ?>
			    			</div>
		    			</div>
		    			
		    			<!-- <form id="contact-form" class="contact-form">
		    				<div class="col-md-6">
		    					<div class="form-group">
								   <input type="text" class="form-control" placeholder="Fullname*" required="required">
								</div>
							    <div class="form-group">
							       <input type="text" class="form-control" placeholder="Subject*" required="required">
							    </div>
							</div>
		    				<div class="col-md-6">
		    					<div class="form-group">
								   <input type="text" class="form-control" placeholder="Email Address*" required="required">
								</div>
							    <div class="form-group">
							       <input type="text" class="form-control" placeholder="Phone Number*" required="required">
							    </div>
		    				</div> 
		    				<div class="col-md-12">
		    					<div class="form-group">
								  <textarea class="form-control" rows="5" id="comment" placeholder="Your Message..."></textarea>
								</div>
								<a type="submit" class="site-btn" href="#">SEND MESSAGE</a>
		    				</div>
		    				
		    			</form> -->
		    		</div>
		    	</div>
		    </section>

		</main><!-- #main -->

	</div><!-- #primary -->

	<?php get_footer( 'becomeblogger' ); ?>


	

