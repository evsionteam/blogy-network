<?php
/* 
	Template Name: Register
*/
	get_header( 'nohead' );
	$blog_notice = get_transient( 'blog_notice' );
	$validation_error = get_transient( 'validation_error' );
	delete_transient( 'blog_notice' );
	delete_transient( 'validation_error' );
	global $bn_opt;
?>

	<div class="register-wrap">
		<div class="container">
			<?php 
				if( isset( $_POST['blogger_registration'] ) ){

					$blogger_categories 	= isset( $_POST['blogger_categories'] ) ? $_POST['blogger_categories'] : array();
						$user_name 	        = $_POST['user_name'];
						$password  	        = $_POST['password'];
						$email     	        = $_POST['email'];
						$first_name         = $_POST['first_name'];
						$last_name          = $_POST['last_name'];

					if( is_wp_error( $blog_notice ) ){
						
						$errors = $blog_notice->get_error_messages();

						if( is_array( $errors) ){
							?>
							<div class="error text-center">
								<div class="alert alert-danger">
									<?php foreach ( $errors as $e ) {
										echo $e;
									} ?>
								</div>
							</div>
							<?php
						}
					}

					if( is_array( $validation_error ) ):
						foreach( $validation_error as $e ):
							?>
							<div class="error text-center">
								<div class="alert alert-danger">
									<?php echo $e; ?>
								</div>
							</div>
						<?php
						endforeach;
					endif;
				}
				
			?>

			<div class="form-registration">
				<div class="brand-logo">
					<a href="#"><img src="<?php echo $bn_opt['logo']['url']; ?>"></a>
				<h2><?php echo $bn_opt['register_title'] ?></h2>
				<p><?php echo $bn_opt['register_desc'];  ?></p>
			    </div>
				<form action="" method="post">
					<?php wp_nonce_field( 'registering_blogger', 'registering_blogger_nonce' ); ?>
					<div class="form-group">
						<input class="form-control" type="text" name="first_name" value="<?php echo isset( $first_name ) ? $first_name : ''; ?>" placeholder="First Name">
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="last_name" value="<?php echo isset( $last_name ) ? $last_name : ''; ?>" placeholder="Last Name">
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="user_name" value="<?php echo isset( $user_name ) ? $user_name : ''; ?>" placeholder="Username">
					</div>

					<div class="form-group">
						<input class="form-control" type="text" name="email" value="<?php echo isset( $email ) ? $email : ''; ?>" placeholder="E mail" required>
					</div>

					<div class="form-group">
						<input class="form-control" type="password" name="password" value="<?php echo isset( $password ) ? $password : ''; ?>" placeholder="Password">
					</div>
					<div class="form-group">
						<?php
							$blog_category = get_terms( array(
							    'taxonomy'   => 'category',
							    'hide_empty' => false,
							    'exclude'    => 1
							));
							
							if( is_array( $blog_category ) ):
								foreach( $blog_category as $cat ):
									if( $cat->name == 'Uncategorized' ) continue;
						?>
							
							<input id="<?php echo $cat->name; ?>" type="checkbox" name="blogger_categories[]" 
							<?php if( isset( $blogger_categories ) && in_array( $cat->name, $blogger_categories) ){
								echo "checked";
								} ?> value="<?php echo $cat->name; ?>" />
								<label for="<?php echo $cat->name; ?>"><?php echo $cat->name; ?></label>
						<?php
								endforeach;
							endif;
						?>
					</div>
					<div class="form-group">
						<input type="submit" name="blogger_registration" class="btn btn-default" value="<?php _e( 'REGISTER','blognetwork '); ?>" />
					</div>
				</form>
			</div><!-- form-registration -->
			<div class="policy-link"><a href="#">Privacy Policy</a></div>
		</div>
	</div>
<?php get_footer(); ?>