<?php if ( post_password_required() ) { return; } ?>

<div class="comments-area <?php echo ( is_user_logged_in() ) ? 'user-logged-in': 'user-not-logged-in' ?>">
	<?php
	if ( have_comments() ) : ?>
		<h3 class="comments-title">
			<?php
				printf( // WPCS: XSS OK.
					esc_html( _nx( '1 comment', '%1$s comments', get_comments_number(), 'comments title', 'blognetwork' ) ),
					number_format_i18n( get_comments_number() ),
					'<span>' . get_the_title() . '</span>'
				);
			?>
		</h3>

		<ul class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'li',
					'short_ping'  => true,
					'avatar_size' => 74,
				) );
			?>
		</ul><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
				<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'blognetwork' ); ?></h2>
				<div class="nav-links">

					<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'blognetwork' ) ); ?></div>
					<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'blognetwork' ) ); ?></div>

				</div><!-- .nav-links -->
			</nav><!-- #comment-nav-below -->
		<?php
		endif; // Check for comment navigation.
	endif; 

	if( is_user_logged_in() ){

		if( ( is_single() ) ){
			$comments_args = array(
		        // change the title of send button 
		        'label_submit'=>'POST COMMENT',
		        // change the title of the reply section
		        'title_reply'=>null,
		        'logged_in_as' => null,
		        'comment_field' =>  '<p class="comment-form-comment"><textarea class="form-control" id="comment" name="comment" aria-required="true"></textarea></p>',
			);
			comment_form( $comments_args);
		}
		else{
			?>
			<a href="<?php the_permalink(); ?>" class="btn btn-default post-comment">
				<?php _e( 'POST COMMENT','blognetwork'); ?>
			</a>
			<?php
		}
	}else{
		?>
		<div class="please-login">
			<a target="_blank" href="<?php echo admin_url(); ?>">
				<?php _e( 'Pleas Login to post comment.','blogger' ); ?>
			</a>
		</div>
		<?php
	}
	?>
</div><!-- #comments -->
