(function( $ ){
	
	function menu( param ){

		this.toggler = param.toggler;
		this.menu = param.menu;

		this.init = function(){

			var that = this;

			$(this.toggler).on( 'click', function(){
				$(that.menu).slideToggle();
			});

			$(window).resize(function(){

				var ww = $(window).width();
				if( ww > 992 ){
					that.show();
				}else{
					that.hide();
				}
			});
		}

		this.hide = function(){
			$( this.menu ).hide();
		}

		this.show = function(){
			$( this.menu ).show();
		}
	}

	$( document ).ready( function(){

		new WOW().init();

		var menuConfig = {
			'menu' : '#menu',
			'toggler' : '.navbar-toggle'
		}

		new menu( menuConfig ).init();
	});

})(jQuery)