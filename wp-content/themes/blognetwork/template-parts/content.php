<?php 
	$post_id = get_the_ID(); 
	$terms = wp_get_post_terms( $post_id, 'category' ); 
?>
<div class="blog-post-list">
	<div class="blog-content-block">
		<article>
			<div class="post-title-wrap">
				<div class="post-category">
					<?php 
						if( is_array( $terms ) ):
							$count = count( $terms );
							foreach ($terms as $key => $value): 
					?>		
								<a href="<?php echo get_term_link ( $value ); ?>"><?php echo $value->name; ?></a>
					<?php
								echo ( $key + 1 !== $count ) ? ',' : '';
							
							endforeach;
						endif;
					?>
				</div>
				<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<div class="post-info"><span><?php the_date(); ?></span></div>
			</div>
			<?php if( has_post_thumbnail() ): ?>
				<a href="<?php the_permalink(); ?>">
					<div class="post-thumbnail" style="background-image:url('<?php the_post_thumbnail_url();?>')">
					</div>
				</a>
			<?php endif; ?>
			<div class="post-content-detail">
				<?php the_content();  ?>
			</div>
			<?php if ( has_post_format( 'video' )):  ?>
				<div class="blog-detail-video">
					<div class="video-img">
						<?php 
							$link = get_post_meta( $post_id, 'video_link', true );
							echo wp_oembed_get($link);
						?>
					</div>
				</div>
			<?php endif; ?>

			<?php
				$likes = get_post_meta( $post_id, 'post_likes',true );
				if( $likes ){
					$total_likes = count( $likes );
				}else{
					$total_likes = 0;
				}
			?>
			<div class="wishlst-count">
				<a href="#" class="wish-list like-it"
					data-user='<?php echo get_current_user_id();  ?>'
					data-post="<?php echo the_ID(); ?>" >
					<span class="fa fa-heart-o like-count"><em><?php echo $total_likes; ?></em></span>
				</a>
				<?php echo do_shortcode('[wp_allshare theme="theme1" networks="facebook,pinterest" /]'); ?>
			</div>
		</article>
	</div>
</div><!-- blog-post-list -->