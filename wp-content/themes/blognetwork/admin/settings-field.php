<?php
    
    if ( ! class_exists( 'Redux' ) ) {
        return;
    }
    $opt_name = 'bn_opt'; 
    $args = array( 'menu_title' => 'Blog Networks Options', 'page_title' => 'Bloggers Options' );
    Redux::setArgs ($opt_name, $args);
    /* General Settings */
    Redux::setSection( $opt_name, array(
        'title' => __( 'General Settings', 'blognetwork' ),
        'id'    => 'general',
        'icon'  => 'el el-home',
        'fields'     => array(
            array(
                'id'       => 'logo',
                'type'     => 'media',
                'title'    => __( 'Logo', 'blognetwork' ),
            ), 
            array(
                'id'       => 'footer_logo',
                'type'     => 'media',
                'title'    => __( 'Footer Logo', 'blognetwork' ),
            ),
            array(
                'id'       => 'header_image',
                'type'     => 'media',
                'title'    => __( 'Header Image', 'blognetwork' ),
            ),
            array(
                'id'       => 'jumbo_btn_text',
                'type'     => 'text',
                'title'    => __( 'Jumbo Button Text', 'blognetwork' ),
                'default'  => 'Explore'
            ),
            array(
                'id'       => 'jumbo_btn_link',
                'type'     => 'text',
                'title'    => __( 'Jumbo Button Link', 'blognetwork' ),
                'default'  => '#'
            ),
            array(
                'id'       => '404_title',
                'type'     => 'text',
                'title'    => __( '404 Title', 'blognetwork' ),
                'default'  => 'Hittades inte, Fel 404'
            ),
            array(
                'id'       => '404_desc',
                'type'     => 'textarea',
                'title'    => __( '404 Description', 'blognetwork' ),
                'default'  => 'Sidan du söker finns inte längre. Kanske kan du gå tillbaka till webbplatsens startsida och se om du kan hitta vad du letar efter.'
            ),
            array(
                'id'       => 'register_title',
                'type'     => 'text',
                'title'    => __( 'Title', 'blognetwork' ),
                'default'  => 'Registration'
            ),
            array(
                'id'       => 'register_desc',
                'type'     => 'textarea',
                'title'    => __( 'Registration Description', 'blognetwork' ),
                'default'  => 'Sidan du söker finns inte längre. Kanske kan du gå tillbaka till webbplatsens startsida och se om du kan hitta vad du letar efter.'
            )
        )
    ) );


    Redux::setSection( $opt_name, array(
        'title' => __( 'Footer Settings', 'blognetwork' ),
        'id'    => 'footer',
        'icon'  => '',
        'fields'     => array(
            array(
                'id'        => 'footer_text',
                'type'      => 'text',
                'title'     => __( 'Footer Text', 'blognetwork' ),
                'default'   => __( 'Copyright &copy; 2016 Eaglevision IT SE', 'blognetwork' ),
            )
        )
    ) );


   $head = array(
         array(
            'id'        => 'head_btn_text',
            'type'      => 'text',
            'title'     => __( 'Head Button Text', 'blognetwork' ),
            'default'   => __( 'BECOME A MEMBER', 'blognetwork' )
        ),
        array(
            'id'        => 'head_btn_link',
            'type'      => 'text',
            'title'     => __( 'Head Button Link', 'blognetwork' ),
            'default'   => __( '#', 'blognetwork' )
        )
    );


    Redux::setSection( $opt_name, array(
        'title'      => __( 'Member Page', 'blognetwork' ),
        'id'         => 'member_page',
        'icon'       => '',
        'fields'     => $head
    ));

    $banner = array(
        array(
            'id'        => 'banner_bg',
            'type'      => 'media',
            'title'     => __( 'Background', 'blognetwork' )
        ),
        array(
            'id'        => 'banner_video',
            'type'      => 'text',
            'title'     => __( 'Video', 'blognetwork' )
        ),
        array(
            'id'        => 'banner_title',
            'type'      => 'text',
            'title'     => __( 'Title', 'blognetwork' ),
            'default'   => __( 'BECOME A Blogger', 'blognetwork' )
        ),
        array(
            'id'        => 'banner_desc',
            'type'      => 'textarea',
            'title'     => __( 'Description', 'blognetwork' ),
            'default'   => __( 'Blogy network is a place there fashion', 'blognetwork' )
        ),
        array( 
            'id'        => 'banner_btn_text',
            'type'      => 'text',
            'title'     => __( 'Button Text', 'blognetwork' ),
            'default'   => __( 'JOIN US TODAY!', 'blognetwork' )
        ),
        array( 
            'id'        => 'banner_btn_text',
            'type'      => 'text',
            'title'     => __( 'Button Text', 'blognetwork' ),
            'default'   => __( 'JOIN US TODAY!', 'blognetwork' )
        ),
        array( 
            'id'        => 'banner_btn_link',
            'type'      => 'text',
            'title'     => __( 'Button Link', 'blognetwork' ),
            'default'   => '#'
        )
    );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Banner', 'blognetwork' ),
        'id'         => 'banner',
        'icon'       => '',
        'subsection' => true,
        'fields'     => $banner
    ));

    $why_us = array(
        array( 
            'id'        => 'why_us_title',
            'type'      => 'text',
            'title'     => __( 'Title', 'blognetwork' ),
            'default'   => 'Why become a blogger with us'
        ),
        array( 
            'id'        => 'why_us_desc',
            'type'      => 'textarea',
            'title'     => __( 'Description', 'blognetwork' ),
            'default'   => 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus suscipit tortor eget felis porttitor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        ),
        array( 
            'id'        => 'why_us_box_1_icon',
            'type'      => 'text',
            'title'     => __( 'Box 1 Icon', 'blognetwork' ),
            'default'   => 'fa fa-setting'
        ),
        array( 
            'id'        => 'why_us_box_1_title',
            'type'      => 'text',
            'title'     => __( 'Box 1 Title', 'blognetwork' ),
            'default'   => 'Get more fans'
        ),array( 
            'id'        => 'why_us_box_1_desc',
            'type'      => 'textarea',
            'title'     => __( 'Box 1 Description', 'blognetwork' ),
            'default'   => 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.'
        ),
        array( 
            'id'        => 'why_us_box_2_icon',
            'type'      => 'text',
            'title'     => __( 'Box 2 Icon', 'blognetwork' ),
            'default'   => 'fa fa-setting'
        ),
        array( 
            'id'        => 'why_us_box_2_title',
            'type'      => 'text',
            'title'     => __( 'Box 2 Title', 'blognetwork' ),
            'default'   => 'Become Famous'
        ),array( 
            'id'        => 'why_us_box_2_desc',
            'type'      => 'textarea',
            'title'     => __( 'Box 2 Description', 'blognetwork' ),
            'default'   => 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.'
        ),
        array( 
            'id'        => 'why_us_box_3_icon',
            'type'      => 'text',
            'title'     => __( 'Box 3 Icon', 'blognetwork' ),
            'default'   => 'fa fa-setting'
        ),
        array( 
            'id'        => 'why_us_box_3_title',
            'type'      => 'text',
            'title'     => __( 'Box 3 Title', 'blognetwork' ),
            'default'   => 'Earn Money'
        ),array( 
            'id'        => 'why_us_box_3_desc',
            'type'      => 'textarea',
            'title'     => __( 'Box 3 Description', 'blognetwork' ),
            'default'   => 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.'
        )

    );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Why us', 'blognetwork' ),
        'id'         => 'why_us',
        'icon'       => '',
        'subsection' => true,
        'fields'     => $why_us
    ));


    Redux::setSection( $opt_name, array(
        'title'      => __( 'Call to Action', 'blognetwork' ),
        'id'         => 'call_to_action',
        'icon'       => '',
        'subsection' => true,
        'fields'     => array(
            array( 
                'id'        => 'cta_title',
                'type'      => 'text',
                'title'     => __( 'Title', 'blognetwork' ),
                'default'   => 'Seen enough? Let\'s get started'
            ), 
            array( 
                'id'        => 'cta_sub_title',
                'type'      => 'text',
                'title'     => __( 'Sub Title', 'blognetwork' ),
                'default'   => 'No Fixed Contract. No Installation Required. Trusted & Secure'
            ),
            array( 
                'id'        => 'cta_btn_title',
                'type'      => 'text',
                'title'     => __( 'Button Text', 'blognetwork' ),
                'default'   => 'Try IT FOR FREE!'
            ), 
            array( 
                'id'        => 'cta_btn_link',
                'type'      => 'text',
                'title'     => __( 'Button Link', 'blognetwork' ),
                'default'   => '#'
            ),
        )
    ));

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Contact', 'blognetwork' ),
        'id'         => 'contact',
        'icon'       => '',
        'subsection' => true,
        'fields'     => array(
            array( 
                'id'        => 'contact_title',
                'type'      => 'text',
                'title'     => __( 'Title', 'blognetwork' ),
                'default'   => 'Get In Touch'
            ),array( 
                'id'        => 'contact_desc',
                'type'      => 'text',
                'title'     => __( 'Description', 'blognetwork' ),
                'default'   => 'There are many variations of passages of Lorem Ipsum available, but the majority
                    have suffered alteration, by injected humour, or new randomised words.'
            )
        )
    ));

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Social Setting', 'blognetwork' ),
        'id'         => 'social_setting',
        'icon'       => '',
        'fields'     => array(
            array( 
                'id'        => 'twitter_link',
                'type'      => 'text',
                'title'     => __( 'Twitter', 'blognetwork' )
            ),array( 
                'id'        => 'pinintrest_link',
                'type'      => 'text',
                'title'     => __( 'Pin Intrest', 'blognetwork' )
            ),array( 
                'id'        => 'fb_link',
                'type'      => 'text',
                'title'     => __( 'Facebook', 'blognetwork' )
            ),array( 
                'id'        => 'instagram_link',
                'type'      => 'text',
                'title'     => __( 'Instagram', 'blognetwork' )
            ),array( 
                'id'        => 'skype_link',
                'type'      => 'text',
                'title'     => __( 'Skype', 'blognetwork' )
            ),array( 
                'id'        => 'dribble_link',
                'type'      => 'text',
                'title'     => __( 'Dribbble', 'blognetwork' )
            )
        )
    ));

